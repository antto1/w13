import logo from './logo.svg';
import './App.css';

function App() {
  //here is an exmple how to create an array
  const Weekday = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  //here is an example how to use the array above to create an array of option elements
  //see https://www.w3schools.com/jsref/jsref_map.asp
  const arrayOfOptionWeekday = Weekday.map((name, position)=>{
    //in JSX language you can combine html elements and js variables this way (see {} around variables)
    return <option key={position} value={position}>{name}</option>

  })
  return (
    <div className="App">
      See the source code how to convert an array to html elements
      <hr/>
      <table>
        <thead>
          <tr>
            <th>Weekday</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>{Weekday[0]}</td>
          </tr>
          <tr>
            <td>{Weekday[1]}</td>
          </tr>
          <tr>
            <td>{Weekday[2]}</td>
          </tr>
          <tr>
            <td>{Weekday[3]}</td>
          </tr>
          <tr>
            <td>{Weekday[4]}</td>
          </tr>
          <tr>
            <td>{Weekday[5]}</td>
          </tr>
          <tr>
            <td>{Weekday[6]}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default App;
